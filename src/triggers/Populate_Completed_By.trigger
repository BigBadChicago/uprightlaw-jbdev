trigger Populate_Completed_By on Case_Task__c (before insert, before update) {

    List<Case_Task__c> CTs_to_update = new List<Case_Task__c>();
    if(Trigger.isInsert){
        for (Case_Task__c ct : Trigger.new){
        	 if(ct.Status__c == 'Completed') {
        		ct.Completed_By__c = UserInfo.getUserId();


        	}
        }
    }

    if(Trigger.isUpdate){
        for (Case_Task__c ct : Trigger.new) {
        //If status was not 'Completed' and is now 'Completed', add to list of CTs to update
        Case_Task__c old_ct = Trigger.oldMap.get(ct.Id);
            if(ct.Status__c == 'Completed' && old_ct.Status__c != 'Completed'){
                ct.Completed_By__c = UserInfo.getUserId();


            }
        }
    }
}
public class RSS {
	public static Map<String, Integer> monthMap = new Map<String, Integer> { 
			'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4, 'May' => 5, 'Jun' => 6
			, 'Jul' => 7, 'Aug' => 8, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12 };
	
	public class channel {
		public String title { get; set; }
		public String link { get; set; }
		public String description { get; set; }
		public String language { get; set; }
/*
		public String author { get; set; }
		public String category { get; set; }
		public String copyright { get; set; }
		public String docs { get; set; }
		public RSS.image image { get; set; }
*/
		public list<RSS.item> items { get; set; }
		public channel() {
			items = new list<RSS.item>();
		}
	}
/*
	public class image {
		public String url { get; set; }
		public String title { get; set; }
		public String link { get; set; }
	}
*/
	public class item {
		public String title { get; set; }
		public String guid { get; set; }
		public String link { get; set; }
		public String description { get; set; }
		public String pubDate { get; set; }
/*
		public String source { get; set; }
*/
		public Date getPublishedDate() {
			Date result = ( pubDate != null ) ? 
							Date.valueOf( pubDate.replace( 'T', ' ' ).replace( 'Z', '' ) ) : null;
			return result;
		}

		public DateTime getPublishedDateTime() {
/*
Fri, 15 May 2015 04:00:00 +0000
*/
			if( pubDate.substring( 3, 4 ) == ',' ) {
				Integer theMonth = monthMap.get( pubDate.substring( 8, 11 ) );

				Date theDate = Date.newInstance( Integer.valueOf( pubDate.substring( 12, 16 ) )
												, theMonth
												, Integer.valueOf( pubDate.substring( 5, 7 ) ) );
				Time theTime = Time.newInstance( Integer.valueOf( pubDate.substring( 17, 19 ) )
												, Integer.valueOf( pubDate.substring( 20, 22 ) )
												, Integer.valueOf( pubDate.substring( 23, 25 ) )
												, Integer.valueOf( pubDate.substring( 27, 31 ) ) );
				DateTime result = DateTime.newInstance( theDate, theTime );
				return result;
			}

			DateTime result = ( pubDate != null ) ? 
								DateTime.valueOf( pubDate.replace( 'T', ' ' ).replace( 'Z', '' ) ) : null;
			return result;
		}
	}

	public static RSS.channel getRSSData( String feedURL ) {
		HttpRequest req = new HttpRequest();
		req.setEndpoint( feedURL );
		req.setMethod( 'GET' );
		
		Dom.Document doc = new Dom.Document();
		Http h = new Http();
		
		if ( ! Test.isRunningTest() ) { 
			HttpResponse res = h.send( req );
			doc = res.getBodyDocument();
		} else {
			String xmlString = '<?xml version="1.0" encoding="utf-8" ?><rss version="2.0" xmlns:os="http://a9.com/-/spec/opensearch/1.1/"><channel><title>salesforce.com - Bing News</title><link>http://www.bing.com/news</link><description>Search Results for salesforce.com at Bing.com</description><category>News</category><os:totalResults>3370</os:totalResults><os:startIndex>0</os:startIndex><os:itemsPerPage>10</os:itemsPerPage><os:Query role="request" searchTerms="salesforce.com" /><copyright>These XML results may not be used, reproduced or transmitted in any manner or for any purpose other than rendering Bing results within an RSS aggregator for your personal, non-commercial use. Any other use requires written permission from Microsoft Corporation. By using these results in any manner whatsoever, you agree to be bound by the foregoing restrictions.</copyright><image><url>http://www.bing.com/s/a/rsslogo.gif</url><title>Bing</title><link>http://www.bing.com/news</link></image><docs>http://www.rssboard.org/rss-specification</docs><item><title>Salesforce.com Makes Friends With CIOs - Information Week</title><guid>http://informationweek.com/news/cloud-computing/software/232602782</guid><link>http://informationweek.com/news/cloud-computing/software/232602782</link><description>Parade of CIOs at CloudForce shows how social networking inroads are making Salesforce.com a larger part of the IT infrastructure. Salesforce.com isn&apos;t just for sales forces anymore. Its Chatter app has opened a social networking avenue into the enterprise ...</description><pubDate>2012-03-19T15:21:47Z</pubDate><source>Information Week</source></item></channel></rss>';
			doc.load( xmlString );
		}
		
		Dom.XMLNode rss = doc.getRootElement();

		// first child element of rss feed is always channel
		Dom.XMLNode channel = rss.getChildElements()[ 0 ];
		
		RSS.channel result = new RSS.channel();
		
		list<RSS.item> rssItems = new list<RSS.item>();
		
		// for each node inside channel
		for( Dom.XMLNode elements : channel.getChildElements() ) {
			String elementName = elements.getName();
			if( 'title' == elementName ) {
				result.title = elements.getText();
			}
			if( 'link' == elementName ) {
				result.link = elements.getText();
			}
			if( 'description' == elementName ) {
				result.description = elements.getText();
			}
/*
			if( 'category' == elementName ) {
				result.category = elements.getText();
			}
			if( 'copyright' == elementName ) {
				result.copyright = elements.getText();
			}
			if( 'docs' == elementName ) {
				result.docs = elements.getText();
			}
			if( 'image' == elementName ) {
				RSS.image img = new RSS.image();

				//for each node inside image
				for( Dom.XMLNode xmlImage : elements.getChildElements() ) {
					if( 'url' == xmlImage.getName() ) {
						img.url = xmlImage.getText();
					}
					if( 'title' == xmlImage.getName() ) {
						img.title = xmlImage.getText();
					}
					if( 'link' == xmlImage.getName() ) {
						img.link = xmlImage.getText();
					}
				}
				result.image = img;
			}
*/
			if( 'item' == elementName ) {
				RSS.item rssItem = new RSS.item();

				// for each node inside item
				for(Dom.XMLNode xmlItem : elements.getChildElements()) {
					String itemName = xmlItem.getName();
					if( 'title' == itemName ) {
						rssItem.title = xmlItem.getText();
					}
					if( 'guid' == itemName ) {
						rssItem.guid = xmlItem.getText();
					}
					if( 'link' == itemName ) {
						rssItem.link = xmlItem.getText();
					}
					if( 'description' == itemName ) {
						rssItem.description = xmlItem.getText();
					}
					if( 'pubDate' == itemName ) {
						rssItem.pubDate = xmlItem.getText();
					}
/*
					if( 'source' == itemName ) {
						rssItem.source = xmlItem.getText();
					}
*/
				}

				// for each item, add to rssItem list
				rssItems.add( rssItem );
			}
			
		}

		// finish RSS.channel object by adding the list of all rss items
		result.items = rssItems;
		
		return result;
		
	}
/*	
	static testMethod void RSSTest() {
		RSS.channel chan = RSS.getRSSData('test');
		Date pDate = chan.items[0].getPublishedDate();
		DateTime pDateTime = chan.items[0].getPublishedDateTime();
	}
*/
}
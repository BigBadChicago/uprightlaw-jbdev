public class My_Tickets_Ctlr {
    public List<Ticket__c> Tickets {
        get; 
        set;
    }
    public My_Tickets_Ctlr(ApexPages.StandardController controller){
        Tickets = [SELECT Id, Name, Case_File__c, CreatedDate, Delegator__c, Status__c, Case_File__r.Name, Delegator__r.Name, Due_Date__c
                   FROM Ticket__c
                   WHERE OwnerId = :Userinfo.getUserId() AND Status__c != 'Closed'];
    }
}
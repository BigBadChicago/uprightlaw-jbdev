public class upr_twilioOutboundCallCustomCtrl {
	String clientDialPhone = null;
	public Case_Task__c cf_CurrentCaseTask { get; set; }
	public Case_File__c cf_CurrentCaseFile { get; set; }
	public TwilioCapability capability;
	public TwilioAccount twiAccount { get; set; }
	public String twiAccountSID { get; set; }
	public String clientCallNotes { get; set; }
	public String clientPhone { get; set; }
    public String clientMobilePhone { get; set; }
    public String clientHomePhone { get; set; }
	public String clientStatePhone { get; set; }
    public String clientStateSMS { get; set; }
	public String SendSMSMessage { get;set; }
	public String txtMessage { get;set; }
	public String status { get; set; }
    public String message { get; set; }
    public String phoneNumber { get; set; }
    public String twilioCallSID { get; set; }
	public Contact caseContact {get; set;}

	public upr_twilioOutboundCallCustomCtrl() {		
		twiAccount = TwilioAPI.getDefaultAccount();
		Id currentRecordId = ApexPages.currentPage().getParameters().get('id');
		String objType = currentRecordId.getSObjectType().getDescribe().getName();
		//cf_CurrentCaseFile = (Case_File__c)controller.getRecord();
		if(objType == 'Case_File__c'){
			cf_CurrentCaseFile = [SELECT Id, Name, Primary_Contact__c, Primary_Contact__r.Phone, Primary_Contact__r.MobilePhone, Primary_Contact__r.HomePhone, Primary_Contact__r.Id, Primary_Contact__r.MailingState, Primary_Account__c, Primary_Account__r.BillingState FROM Case_File__c WHERE Id =: currentRecordId LIMIT 1];
		} else if(objType == 'Case_Task__c'){
			cf_CurrentCaseTask = [SELECT Id, Name, Case_File__c, Case_File__r.Id FROM Case_Task__c WHERE Id =: currentRecordId LIMIT 1];
			cf_CurrentCaseFile = [SELECT Id, Name, Primary_Contact__c, Primary_Contact__r.Phone, Primary_Contact__r.MobilePhone, Primary_Contact__r.HomePhone, Primary_Contact__r.Id, Primary_Contact__r.MailingState, Primary_Account__c, Primary_Account__r.BillingState FROM Case_File__c WHERE Id =: cf_CurrentCaseTask.Case_File__r.Id LIMIT 1];
		}
		
		System.debug('Page Object Type: ' + objType);
 		clientPhone = cf_CurrentCaseFile.Primary_Contact__r.Phone;
        clientMobilePhone = cf_CurrentCaseFile.Primary_Contact__r.MobilePhone;
		List<State__c> cf_StateList  = [SELECT Id, Name, Abbreviation__c, Outbound_CallerID__c, State_SMS_Number__c FROM State__c WHERE Abbreviation__c = :cf_CurrentCaseFile.Primary_Account__r.BillingState];
		clientStatePhone = cf_StateList[0].Outbound_CallerID__c;
        clientStateSMS = cf_StateList[0].State_SMS_Number__c;		
        capability = TwilioAPI.createCapability();
        capability.allowClientOutgoing(TwilioAPI.getTwilioConfig().ApplicationSid__c);
		
	}

	public String getToken() { return capability.generateToken(); }

	public List<SelectOption> getContactPhones() {
		List<SelectOption> options = new List<SelectOption>();
		if(clientPhone != null && clientPhone != ''){
			options.add(new SelectOption( clientPhone,'Phone: '+ clientPhone));
		}
		if(clientMobilePhone != null && clientMobilePhone != ''){
			options.add(new SelectOption( clientMobilePhone,'Cell Phone: '+ clientMobilePhone));
		}
		if(clientHomePhone != null && clientHomePhone != ''){
			options.add(new SelectOption( clientHomePhone,'Home Phone: '+ clientHomePhone));
		}
		return options;
	}

	public String getClientDialPhone() {
		return clientDialPhone;
	}

	public void setClientDialPhone(String clientDialPhone) {
		this.clientDialPhone = clientDialPhone;
	}

}
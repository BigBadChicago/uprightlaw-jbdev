public without sharing class TeamAssignerCtrlr {
    
    @AuraEnabled
    public static List<User> getListOfActiveFRs (){
        //This code and test code would need updating if we change the Profile Name of our First Responders.
        
        List<User> usr_list =[SELECT Id, Name, Team__c FROM User 
                              WHERE IsActive = TRUE AND Profile.Name ='First Responder (Force.com)'
                              ORDER BY Name ASC];
        
        return usr_list;
    }
    
    @AuraEnabled
    public static void updateTeams (List<User> updatedList){
        update updatedList;
    }
    
    
    
}
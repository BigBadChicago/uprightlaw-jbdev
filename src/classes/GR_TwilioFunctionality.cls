public class GR_TwilioFunctionality {
	
    @AuraEnabled
    public static TwilioAccount getTwilioAccount(){
        TwilioAccount ta = TwilioAPI.getDefaultAccount();
        return ta;
    }
    
    //@AuraEnabled
    public string makeTwilioCall(String clientPhone, String attorneyPhone, String contactID) { 
        System.debug('****JB_GRTF**** Client Phone is: '+ clientPhone);
        System.debug('****JB_GRTF**** Attorney Phone is: '+ attorneyPhone);
        System.debug('****JB_GRTF**** Contact ID is: '+ contactID);
        TwilioRestClient client = TwilioAPI.getDefaultClient();
        
        String s1 = attorneyPhone.remove('(');
        String s2 = s1.remove(')');
        String s3 = s2.remove('-');
        attorneyPhone = '+1' + s3.deleteWhitespace();
         
        String attyPhoneEncoded = EncodingUtil.urlEncode(attorneyPhone, 'UTF-8');
        
		Map<String, String> params = new Map<String, String>{
            'From' => '+18884089779',
            'To' => clientPhone,
            'Url' => 'https://paconnect.herokuapp.com/call/welcome/' + attyPhoneEncoded + '/' + contactID,
            'Method' => 'POST',
            'IfMachine' => 'Continue',
            'StatusCallback' => 'https://paconnect.herokuapp.com/call/byebye/',
            'Timeout' => '225'
        };
        TwilioCall call = client.getAccount().getCalls().create(params); 
        return call.getSid();
    } 
    
    
    
}
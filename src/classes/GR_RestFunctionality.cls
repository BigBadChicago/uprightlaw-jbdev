@RestResource(urlMapping='/paconnect/*')
global class GR_RestFunctionality {
    
    
    // @HttpGet
    // global static List<Contact> getContacts() {
    //     List<Contact> contacts = [SELECT Id, Name from Contact];
    //     return contacts;
    // }
    
    @HttpPost
    global static void getNextCall() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String contactId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        //List<Contact> userContacts = [SELECT Id, Name, Related_User__c FROM Contact WHERE RecordType.Name = 'Partner Attorney' AND Related_User__c =: UserInfo.getUserId()];
        //Contact userContact = userContacts[0];
        List<App_Session__c> appSessions = [SELECT Id, Name, AppSessionStartTime__c, AppSessionEndTime__c, PA_Selected_Call_Types__c, PA_Selected_Phone__c, Session_Owner__c, Session_Owner__r.Related_User__c FROM App_Session__c WHERE Session_Owner__c =: contactId ORDER BY SystemModStamp ASC NULLS LAST LIMIT 25];
        App_Session__c appSession = appSessions[0];
        String callTypes = appSession.PA_Selected_Call_Types__c;
        List<String> callTypesList = callTypes.split(';');
        String callTypesJSON = JSON.serializePretty(callTypesList);
        // PageReference pgRef = Page.GR_FetchNextCallPage;
        // pgRef.setRedirect(true);
        // return pgRef;
        
        
        //GoReadyContainerCtrl.fetchCall(callTypesJSON);
    }
    
    // http://salesforce.stackexchange.com/questions/44142/how-can-i-login-into-salesforce-from-salesforce-lets-say-through-apex-code-or-r
    
    
    @HttpGet
    global static String doDNC() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        //String contactId = RestContext.request.params.get('Id');
        String contactId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Contact result = [SELECT Id, Name, Phone, MobilePhone, HomePhone, OtherPhone, DNC_Auto_Dial__c FROM Contact WHERE Id =: contactId LIMIT 1];
        result.DNC_Auto_Dial__c = true;
        update result;
        res.addHeader('Content-Type', 'text/xml');
        String jsonReturn = JSON.serializePretty('Success');
        return jsonReturn;
    }
    
    @HttpPut
    global static void doDNCAutoCall(String cid) {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        //String contactId = RestContext.request.params.get('Id');
        String contactId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Contact result = [SELECT Id, Name, Phone, MobilePhone, HomePhone, OtherPhone, DNC_Auto_Dial__c FROM Contact WHERE Id =: contactId LIMIT 1];
        result.DNC_Auto_Dial__c = true;
        update result;
    }
}
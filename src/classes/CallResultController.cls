public with sharing class CallResultController {
    @AuraEnabled
    public static String logResultSendTextMessage(String prospectId, String callId){
        TwilioSMSCtrl tsc = new TwilioSMSCtrl();
        return tsc.sendProspectTextMessage(callId, prospectId);
    }
    
    @AuraEnabled
    public static void logResultUpdateProspect(String incomingProspectID, String msgListNumber){
        //public static void logResultUpdateProspect(Integer msgListNumber, String incomingProspectID){
        TwilioSMSCtrl tsc = new TwilioSMSCtrl();
        SYSTEM.debug('***JB1** msgListNumber: ' + msgListNumber);
        SYSTEM.debug('***JB1** incomingProspectID: ' + incomingProspectID);
        tsc.updateProspectPostSMS(msgListNumber, incomingProspectID);
    }
    
    
    @AuraEnabled
    public static Task createActivity(Id prospectId, Id callId, String notes, String why, String hope, String debts){
        
        
        Task tsk = new Task();
        tsk.Subject = 'Call to Prospect'; 
        tsk.Status = 'Completed';
        tsk.OwnerId = UserInfo.getUserId();
        tsk.WhatId = prospectId;
        tsk.Call__c = callId;
        tsk.ActivityDate = date.today(); 
        tsk.Description = notes;
        
        SYSTEM.debug('CallResultController.createActivity >> prospectId: ' + prospectId);
        Prospect__c prp = [SELECT Id, Why__c, Facts_about_Debt__c, Hope__c, Base_Score__c FROM Prospect__c WHERE Id =: prospectId];
        SYSTEM.debug('CallResultController.createActivity >> prpId: ' + prp.Id);		
        prp.Why__c = why;
        prp.Facts_about_Debt__c = debts;
        prp.Hope__c = hope;
        if(prp.Base_Score__c != null){
            prp.Base_Score__c -= 5;
        }
        update prp;
        
        Call__c call = [SELECT Id, Status__c, Completed__c, Call_Pattern__c FROM Call__c WHERE Id =: callId];
        call.Status__c = 'Completed';
        call.Completed__c = Datetime.now();
        call.Completed_By__c = UserInfo.getUserId();
        update call;
        
        System.debug('CallResultController.createActivity >> callPatternId: ' + call.Call_Pattern__c);
        Call_Pattern__c pattern = [SELECT Id, Calls__c from Call_Pattern__c WHERE Id =: call.Call_Pattern__c];
        if (pattern.Calls__c == null){
            pattern.Calls__c = 1;
        }
        else{
            pattern.Calls__c++;
        }
        update pattern;
        
        insert tsk;
        return tsk;
        
    }
    
    @AuraEnabled
    public static void updateDNC(Id prospectId, Boolean dnc){
        List<Prospect__c> prospects = [SELECT Id, DNC__c FROM Prospect__c WHERE Id =: prospectId];
        Prospect__c p = prospects[0];
        p.DNC__c = dnc;
        update p;
        
    }
    
    @AuraEnabled
    public static String sendEmailToProspect(String callId, String prospectId){
        
        Prospect__c prospect = [SELECT Name, Personal_Email__c, First_Name__c
                                FROM Prospect__c
                                WHERE Id =: prospectId
                                LIMIT 1];
        
        Call__c prospectCall = [SELECT Name, Call_Template__c, Email_Template__c
                                FROM Call__c
                                WHERE Id =: callId
                                LIMIT 1];
        
        String templateId = prospectCall.Call_Template__c;
        
        Call_Template__c callTemp = [SELECT Name, Email_Template__c
                                     FROM Call_Template__c
                                     WHERE Id =: templateId
                                     LIMIT 1];
        
        String emailTemplateId = callTemp.Email_Template__c;
        //next check if there is an email template attached to call template
        if (emailTemplateId != null){
            
            Email_Template__c emailTemp = [SELECT Name, Subject__c, From__c, HTML_Email__c
                                           FROM Email_Template__c
                                           WHERE Id=:emailTemplateId
                                           LIMIT 1];
            
            //set variables from SOQL queries
            String prospName = prospect.First_Name__c;                    
            String fromAddress = emailTemp.From__c;
            String subject = emailTemp.Subject__c;
            String msgBody = emailTemp.HTML_Email__c;
            
            System.debug('msgBody from email Template = '+msgBody);
            
            //convert string of escaped HTML to readable and parsable HTML
            msgBody = msgBody.unescapeHtml4();
            
            System.debug('msgBody after unescape = '+msgBody);
            
            //set merge field for prospect name
            msgBody = msgBody.replace('{!Prospect.FirstName}',prospName);
            
            System.debug('msgBody after replace = '+msgBody);
            
            OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'noreply@uprightlaw.com'];
            
            
            //construct the email message
            Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
            msg.setSubject(subject);
            msg.setReplyTo(fromAddress);
            msg.setToAddresses(new String[] {prospect.Personal_Email__c});
            msg.setHtmlBody(msgBody);
            
            if(owea.size()>0){
                msg.setOrgWideEmailAddressId(owea[0].Id);
            }
            else{
                msg.setSenderDisplayName('UpRight Law');
            }
            
            
            //create a new instance of the Messaging.SingleEmailMessage class
            Messaging.SingleEmailMessage[] messages = 
                new Messaging.SingleEmailMessage[]{ msg };
                    
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            if (results[0].isSuccess()) {
                System.debug('responderEmailSend >> '+emailTemp.Name +' Successfully sent ');
                string endMessage = 'SUCCESS';
                
                Task tsk = new Task();
                tsk.Subject = 'Email to Prospect'; 
                tsk.Status = 'Completed';
                tsk.Type = 'Email Sent';
                tsk.OwnerId = UserInfo.getUserId();
                tsk.WhatId = prospectId;
                tsk.Call__c = callId;
                tsk.ActivityDate = date.today(); 
                tsk.Description = emailTemp.Name+' Email Template sent to Prospect\'s email: '+prospect.Personal_Email__c+'.';
                insert tsk;
                
                prospectCall.Email_Template__c = emailTemplateId;
                update prospectCall;
                
                return endMessage;
                
            } 
            else {
                System.debug('responderEmailSend >>  Failed sending email ');
                string endMessage = 'ERROR';
                return endMessage;
            }
        }
        else{
            string endMessage = 'No Email Template attached for send';
            return endMessage;
        }
    }
    
    
}
public with sharing class VFC_RefreshDocketEntries {
	public Case_File__c theCaseFile;

	public VFC_RefreshDocketEntries( ApexPages.StandardController controller ) {
		if( ! Test.isRunningTest() ) {
			controller.addFields( new LIST<String> { 'Case_Number__c', 'RSS_URL__c'
					, 'BK_District__c', 'Long_Case_Number__c' } );
		}
		theCaseFile = (Case_File__c) controller.getRecord();
		if( theCaseFile == null ) {
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR
					, 'Could not find case file.' ) );
			return;
		}

		if( theCaseFile.Case_Number__c == null ) {
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR
					, 'Case number is blank.' ) );
			return;
		}

	}

	public PageReference refreshDocketEntries() {
		if( theCaseFile.RSS_URL__c == null ) {
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR
					, 'RSS URL is blank. Retrieval in progress. Please try again in a minute.' ) );

			Set<String> courtSet = new Set<String>();
			courtSet.add( theCaseFile.BK_District__c );
			List<Case_File__c> newCaseFileList = new List<Case_File__c>();
			newCaseFileList.add( theCaseFile );
			List<String> errorList = new List<String>();
			SchedulableDocketEntriesRefresh.setCourtCodesAndFindFeeds( 
						courtSet, newCaseFileList, errorList );
			return null;
		}

		List<Docket_Entry__c> newDocketEntryList = new List<Docket_Entry__c>();
		SchedulableDocketEntriesRefresh.addDocketEntries( theCaseFile.ID, theCaseFile.RSS_URL__c, newDocketEntryList );

		// save docket entries
		List<String> errorList = new List<String>();
		if( newDocketEntryList.size() > 0 ) {
			errorList = SchedulableDocketEntriesRefresh.saveDocketEntries( newDocketEntryList, errorList );
		}

		if( errorList.size() == 0 ) {
			ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO
				, 'Docket entries have been updated from Inforuptcy.' ) );
		} else {
			for( String anError : errorList ) {
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR
					, anError ) );
			}
		}

		return null;
	}
}
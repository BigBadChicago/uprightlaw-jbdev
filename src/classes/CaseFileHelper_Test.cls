@isTest
private class CaseFileHelper_Test {
	// NOTE:  this class provides test coverage for CaseFileHelper, RSS
	// and SchedulableDocketEntriesRefresh

	@TestSetup
	public static void createCourts() {
		// create court record
		Bankruptcy_Court__c bc = new Bankruptcy_Court__c();
		bc.BK_District__c = 'District of Columbia';
		bc.Court_Code__c = 'dcbke';
		insert bc;
	}

	public static TestMethod void myUnitTest() {
		Test.startTest();

		// create case files
		List<Case_File__c> l = new List<Case_File__c>();
		Case_File__c c = new Case_File__c();
		c.BK_District__c = 'District of Columbia';
		c.Case_Number__c = '12:15-0346-7';
		c.Stage__c = 'Filed';
		l.add( c );

		// test wrong district 
		Case_File__c c2 = new Case_File__c();
		c2.BK_District__c = 'Test BK court not found';
		c2.Case_Number__c = '00-0000';
		c2.Stage__c = 'Filed';
		l.add( c2 );

		// test blank case number
		Case_File__c c3 = new Case_File__c();
		c3.BK_District__c = 'District of Columbia';
		c3.Stage__c = 'Filed';
		l.add( c3 );
		insert l;

		// test update
		c3.BK_District__c = 'District of Columbia';
		c3.Case_Number__c = '12:15-0346-7';
		update c3;

		Test.stopTest();

		c = [ SELECT ID, RSS_URL__c FROM Case_File__c WHERE ID = :c.ID ];

		system.assertEquals( SchedulableDocketEntriesRefresh.TEST_URL, c.RSS_URL__c, 'Test RSS retrieval' );
	}

	public static TestMethod void scheduleBatchTest() {
		// create case file
		List<Case_File__c> l = new List<Case_File__c>();
		Case_File__c c = new Case_File__c();
		c.Name = '';
		c.BK_District__c = 'District of Columbia';
		c.Case_Number__c = '12:15-0346-7';
		c.Stage__c = 'Filed';
		l.add( c );
		insert l;

		// test batch execution
		SchedulableDocketEntriesRefresh s = new SchedulableDocketEntriesRefresh();
		s.startBatch();
		s.executeBatch( l );

		s.errorList.add( 'Test' );
		s.finishBatch();

		List<Docket_Entry__c> dockets = new List<Docket_Entry__c>();
		SchedulableDocketEntriesRefresh.addDocketEntries( c.ID, '', dockets );
		SchedulableDocketEntriesRefresh.saveDocketEntries( dockets, s.errorList );
		SchedulableDocketEntriesRefresh.saveCaseFiles( l, s.errorList );
	}

	public static TestMethod void testRefreshPage() {
		Case_File__c c = new Case_File__c();
		c.Name = '';
		c.BK_District__c = 'District of Columbia';
		c.Case_Number__c = '12:15-0346-7';
		c.Stage__c = 'Filed';
		c.RSS_URL__c = 'test.com';
		insert c;

		ApexPages.StandardController stdCtrlr = 
					new ApexPages.StandardController( c );
		VFC_RefreshDocketEntries controller = new VFC_RefreshDocketEntries( stdCtrlr );
		controller.refreshDocketEntries();

		Case_File__c c1 = new Case_File__c();
		c1.Name = '';
		c1.BK_District__c = 'District of Columbia';
		c1.Stage__c = 'Filed';
		insert c1;
		stdCtrlr = new ApexPages.StandardController( c1 );
		controller = new VFC_RefreshDocketEntries( stdCtrlr );
		controller.refreshDocketEntries();
	}
}
@isTest
public class TeamAssignerTestClass {
    
    private static List<User> queryUsers(){
        //This test code and original code would need updating if we change the Profile Name of our First Responders.
        List<User> usr_list =[SELECT Id, Name, Team__c FROM User 
                              WHERE IsActive = TRUE AND Profile.Name ='First Responder (Force.com)'
                              ORDER BY Name ASC];
        
        return usr_list;
    }
    
    @isTest static void fetchUserList(){
        
        test.startTest();
        List<User> usr_list = TeamAssignerCtrlr.getListOfActiveFRs(); 
        test.stopTest();
        
        System.assertNotEquals(null, usr_list[0].Id);
        
    }
    
    @isTest static void pushUserList(){
        List<User> usersList = queryUsers();
        
        test.startTest();
        TeamAssignerCtrlr.updateTeams(usersList); 
        test.stopTest();
        
    }
    
}
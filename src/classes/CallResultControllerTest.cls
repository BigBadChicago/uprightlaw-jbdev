@isTest
public class CallResultControllerTest {
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='Test Prospect'+i, Postal_Code__c = postal_code, First_Name__c = 'Test', Personal_Email__c = 'techops@uprightlaw.com');
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    private static List<Call__c> createCalls(List<Prospect__c> prospects){
        
         //Takes list of Propsects created by method above and creates Call records for each
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        cp.Calls__c = null;
        cp.Consults__c = null;
            
        insert cp;

        SMS_Template__c sms = new SMS_Template__c();
        sms.Name = 'Supercool';
        sms.SMS_Message__c = 'Lorem ipsum dolor sit amet';
        
        insert sms;
        
        Email_Template__c eml = new Email_Template__c();
        eml.Name = 'McLovin';
        eml.Subject__c = 'Thats like slapping God across the face';
        eml.From__c = 'longdofthelaw@uprightlaw.com';
        eml.HTML_Email__c = 'Hi {!Prospect.FirstName} Lorem ipsum dolor sit amet';
        
        insert eml;
        
        Call_Template__c ct = new Call_Template__c(); 
        ct.Call_Pattern__c = cp.Id;
        ct.Attempt__c = 1;
        ct.Min_Hours_Since_Previous_Call__c = 0;
        ct.Team__c = 'A';
        ct.SMS_Template__c = sms.Id;
        ct.Email_Template__c = eml.Id;
        insert ct;
        
        
        
        List<Call__c> calls = new List<Call__c>();
        for(Prospect__c p : prospects){
            Call__c c = new Call__c();
            c.Prospect__c = p.Id;
            c.Call_Pattern__c = cp.Id;
            c.Call_Template__c = ct.Id;
            c.Status__c = 'Not Started';
            c.Attempt__c = 1;
            c.Earliest_Call_Time__c = System.now();
            c.Team__c = 'A';
            calls.add(c);
        }
        insert calls;
        return calls;
    }                        
    
    @isTest static void testCreateActivity(){
        
        List<Prospect__c> prospects = createProspects(1, '60630');
        List<Call__c> calls = createCalls(prospects);
        System.debug('**** CreateActivity TestClass: prospectID >> ' + prospects[0].Id);
        System.debug('**** CreateActivity TestClass: callId >> ' + calls[0].Id);

        
        test.startTest();
        Task tsk = CallResultController.createActivity(prospects[0].Id, calls[0].Id, 'asdf', 'asdf', 'asdf', 'asdf');
        test.stopTest();
        
        Call_Pattern__c cp_post = [SELECT Calls__c FROM Call_Pattern__c WHERE Id =: calls[0].Call_Pattern__c];
        System.assertNotEquals(null, tsk.Id);
        System.assertEquals(1, cp_post.Calls__c);
        
        
    }
    
    
    //Call_Pattern__c pattern = [SELECT Id, Prospects__c FROM Call_Pattern__c WHERE Active__c = true ORDER BY CreatedDate desc LIMIT 1];
    //Call_Template__c template = [SELECT Id, Team__c FROM Call_Template__c WHERE Call_Pattern__c =: pattern.Id AND Attempt__c = 1];

    @isTest static void testSendFREmail(){
        
        List<Prospect__c> prospects = createProspects(1, '60630');
        List<Call__c> calls = createCalls(prospects);
        System.debug('**** CreateActivity TestClass: prospectID >> ' + prospects[0].Id);
        System.debug('**** CreateActivity TestClass: callId >> ' + calls[0].Id);

        
        test.startTest();
        String endMsg = CallResultController.sendEmailToProspect(calls[0].Id, prospects[0].Id);
        test.stopTest();
        
        System.assertEquals(endMsg, 'SUCCESS');        
    }
}
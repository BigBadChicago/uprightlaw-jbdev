@isTest
private class Populate_Time_Zone_Test_Class {
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){
        
        
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            prospects.add(p); 
        }
        insert prospects;
        return prospects;
    }
    
    
    //Test that Zip_Code and Time_Zone are not null with valid postal_code after insert
    @isTest static void testPopulateZipAndTzOnInsert() {
        
        test.startTest();
        List<Prospect__c> prospects = createProspects(1, '60603');
        
        for(Prospect__c p: prospects){
            System.debug('Postal_Code >> ' + p.Postal_Code__c);
            System.debug('ZipCodeId >> ' + p.Zip_Code_Lookup__c);
            
            Prospect__c testp = [SELECT Zip_Code_Lookup__c, Time_Zone_Lookup__c FROM Prospect__c WHERE ID =: p.Id];
            List<Zip_Code__c> zips = [SELECT Name FROM Zip_Code__c WHERE Id =: testp.Zip_Code_Lookup__c];
            String zip = zips[0].Name;
            
            
            
            //Assert that Zip_Code_Lookup__c and Time_Zone_Lookup__c have been populated
            System.assertNotEquals(null, testp.Zip_Code_Lookup__c);
            System.assertNotEquals(null, testp.Time_Zone_Lookup__c);
            
            
            
            //Test that Zip_Code.name == Postal_Code after update of Postal Code
            System.assertEquals(zip, p.Postal_Code__c);
            
            
        }
        test.stopTest();
    }
    
    
    
    
    
    //Test that Zip_Code.name == Postal_Code after update of Zip_Code
    
    //Test that Zip_Code.name == '90210' after insert with invalid Postal_Code
    
    @isTest static void testInvalidZip() {
        
        test.startTest();
        List<Prospect__c> prospects = createProspects(1, '12345');
        
        for(Prospect__c p: prospects){
            System.debug('Postal_Code >> ' + p.Postal_Code__c);
            System.debug('ZipCodeId >> ' + p.Zip_Code_Lookup__c);
            
            Prospect__c testp = [SELECT Zip_Code_Lookup__c, Time_Zone_Lookup__c FROM Prospect__c WHERE ID =: p.Id];
            
            //Assert that Zip_Code_Lookup__c and Time_Zone_Lookup__c have been populated
            System.assertNotEquals(null, testp.Zip_Code_Lookup__c);
            System.assertNotEquals(null, testp.Time_Zone_Lookup__c);
            
            List<Zip_Code__c> zips = [SELECT Name FROM Zip_Code__c WHERE Id =: testp.Zip_Code_Lookup__c];
            String zip = zips[0].Name;
            
            //Test that Zip_Code.name == Postal_Code after update of Postal Code
            System.assertEquals(zip, '90210');
            
            test.stopTest();
        }
    }
    
    @isTest static void testPopulateZipAndTzOnUpdate() {
        
        test.startTest();
        List<Prospect__c> prospects = createProspects(1, '60603');
        
        Prospect__c p = prospects[0]; 
        System.debug('Postal_Code >> ' + p.Postal_Code__c);
        System.debug('ZipCodeId >> ' + p.Zip_Code_Lookup__c);        
        
        //Change postal code and confirm that new zip.name = updated postal_code
        
        p.Postal_Code__c = '90210';
        update p;
        
        Prospect__c testp = [SELECT Zip_Code_Lookup__c, Time_Zone_Lookup__c FROM Prospect__c WHERE ID =: p.Id];
        Zip_Code__c testzip = [SELECT Name, Id FROM Zip_Code__c WHERE Id =: testp.Zip_Code_Lookup__c];
        
        System.assertEquals('90210', testzip.Name);
        
        
        //Change Time_Zone id and confirm that postal_code = newZip.name
        Zip_Code__c updated_zip = [SELECT Id, Name FROM Zip_Code__c WHERE Name = '60603'];        
        p.Zip_Code_Lookup__c = updated_zip.Id;
        update p;
        
        Prospect__c testp2 = [SELECT Zip_Code_Lookup__c, Postal_Code__c FROM Prospect__c WHERE ID =: p.Id];
        Zip_Code__c testzip2 = [SELECT Name, Id FROM Zip_Code__c WHERE Id =: testp2.Zip_Code_Lookup__c];
        
        System.assertEquals(testp2.Postal_Code__c, testzip2.Name);
        
        test.stopTest();
    }  
    
    
    @isTest static void testInvalidZipOnUpdate() {
        
        test.startTest();
        List<Prospect__c> prospects = createProspects(1, '60603');
        
        for(Prospect__c p: prospects){
            
            p.Postal_Code__c = '12345';
            update p;
            
            Prospect__c testp = [SELECT Zip_Code_Lookup__c, Time_Zone_Lookup__c FROM Prospect__c WHERE ID =: p.Id];
            
            
            List<Zip_Code__c> zips = [SELECT Name FROM Zip_Code__c WHERE Id =: testp.Zip_Code_Lookup__c];
            String zip = zips[0].Name;
            
            //Test that Zip_Code.name == Postal_Code after update of Postal Code
            System.assertEquals(zip, '90210');
            
            test.stopTest();
        }
    }    
    
}
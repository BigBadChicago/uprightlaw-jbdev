public with sharing class ActivityStreamCntrlr {
@AuraEnabled
    public static List<Task> fetchTasks(String prospectId){
        List<Task> tasks = [SELECT Id, Subject, Status, Description, ActivityDate, CreatedDate, Owner.Name, Type, WhoId, WhatId FROM Task WHERE WhatId= :prospectId AND CreatedDate >= LAST_N_DAYS:3 ORDER BY CreatedDate DESC LIMIT 15];
    	System.debug(tasks);
    	System.debug(prospectId);
        return tasks;
    }
}
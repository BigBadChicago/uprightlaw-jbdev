@isTest
public class Deal_Marked_as_Hired_TestClass {
    
    private static List<Prospect__c> createProspects(Integer numProspects, String postal_code){                                
        //Accepts an integer for # of Prospects to creat and a String for the postal_code 
        //Creates TZ and Zip Code records for the provided postal_code as well as for '90210'
        
        Call_Pattern__c cp = new Call_Pattern__c();
        cp.Active__c = true;
        cp.Name = 'Test_Call_Pattern';
        cp.Unlimited_Calls__c = true;
        cp.Calls__c = null;
        cp.Consults__c = null;
        cp.Hires__c = null;
        
        insert cp;
        
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
        insert tz;
        if(postal_code != '12345'){
            Zip_Code__c z = new Zip_Code__c(Name=postal_code, Time_Zone__c = tz.Id);            
            insert z;
        }
        
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
        insert test_tz;
        Zip_Code__c test_z = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z;
        
        List<Prospect__c> prospects = new List<Prospect__c>(); 
        for (Integer i=0; i<numProspects; i++){
            Prospect__c p = new Prospect__c(Name='TestProspect'+i, Postal_Code__c = postal_code);
            p.Call_Pattern__c = cp.Id;
            prospects.add(p); 
        }
        
        insert prospects;
        return prospects;
    }
    
    //Create Deal for each prospect in createProspects
    //insert Deal as Stage == Hired and assert that cp.Hires = 1
    //insert Deal as Stage == Not Started, update to Hired, assert that cp.Hires == 1
    
    @isTest
    static void insertDealAsHired(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        Prospect__c p = prospects[0];
        
        test.startTest();
        Deal__c d = new Deal__c();
        d.Name = 'TestDeal';
        d.Stage__c	= 'Hired'; 
        d.Prospect__c = p.Id;
        insert d;        
        test.stopTest();
        
        Prospect__c test_p = [SELECT Id, Call_Pattern__c, Hired__c FROM Prospect__c WHERE Id =: p.Id];
        SYSTEM.debug(test_p.Call_Pattern__c); 
        List<Call_Pattern__c> test_cps = [SELECT Id, Hires__c, Calls__c, Consults__c  FROM Call_Pattern__c WHERE Id =: test_p.Call_Pattern__c];
		Call_Pattern__c test_cp = test_cps[0];
        System.debug(test_cp);
        System.assertEquals(1, test_cp.Hires__c);
        System.assertNotEquals(null, test_p.Hired__c);
    }
    
    @isTest
    static void updateDealToHired(){
        List<Prospect__c> prospects = createProspects(1, '60603');
        Prospect__c p = prospects[0];
        
        test.startTest();
        Deal__c d = new Deal__c();
        d.Name = 'TestDeal';
        d.Stage__c	= 'Not Started'; 
        d.Prospect__c = p.Id;
        insert d;       
        
        d.Stage__c = 'Hired';
        update d;
        test.stopTest();
        
        Prospect__c test_p = [SELECT Id, Call_Pattern__c, Hired__c FROM Prospect__c WHERE Id =: p.Id];
        SYSTEM.debug(test_p.Call_Pattern__c); 
        List<Call_Pattern__c> test_cps = [SELECT Id, Hires__c, Calls__c, Consults__c  FROM Call_Pattern__c WHERE Id =: test_p.Call_Pattern__c];
		Call_Pattern__c test_cp = test_cps[0];
        System.debug(test_cp);
        System.assertEquals(1, test_cp.Hires__c);
        System.assertNotEquals(null, test_p.Hired__c);
    }
 
    
}
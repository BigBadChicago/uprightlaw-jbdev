public class GoReadyContainerCtrl {

    //GR_TwilioFunctionality GRTF = new GR_TwilioFunctionality();
    
    @AuraEnabled
    public static List<String> fetchRawPANumbersList(){
        List<String> PANumbers = new List<String>();
        List<Contact> usrContacts = [SELECT Id, Name, Related_User__c, Phone, MobilePhone, HomePhone, OtherPhone FROM Contact WHERE RecordType.Name = 'Partner Attorney' AND Related_User__c =: UserInfo.getUserId() LIMIT 1];
        PANumbers.add(usrContacts[0].Phone);
        PANumbers.add(usrContacts[0].MobilePhone);
        PANumbers.add(usrContacts[0].HomePhone);
        PANumbers.add(usrContacts[0].OtherPhone);
        return PANumbers;

    }
    
    
    @AuraEnabled
    public static void startApplicationSessionClock(String attyPhone, String callTypesJSON ) {
        System.debug('****JB**** Attorney Phone is: '+ attyPhone);
        System.debug('****JB**** Call Types JSON is: '+ callTypesJSON);
        //List<Object> callTypes = (List<Object>)JSON.deserializeUntyped(callTypesJSON);
        List<String> callTypes = (List<String>)JSON.deserialize(callTypesJSON, List<String>.class);
        System.debug('callTypes='+ callTypes);
        System.debug('callTypes Size='+ callTypes.size());
        App_Session__c appSess = new App_Session__c();
        appSess.AppSessionStartTime__c = dateTime.now();
        List<User> usrList = [SELECT Id, Name FROM USER WHERE Id =: UserInfo.getUserId() LIMIT 1];
        List<Contact> usrContacts = [SELECT Id, Name, Related_User__c FROM Contact WHERE RecordType.Name = 'Partner Attorney' AND Related_User__c =: UserInfo.getUserId() LIMIT 1];
        appSess.Session_Owner__c = usrContacts[0].Id;
        appSess.Application__c = 'Go Ready';
        appSess.App_SessionId__c = userInfo.getSessionId();
        appsess.PA_Selected_Phone__c = attyPhone;
        appSess.PA_Selected_Call_Types__c = callTypesJSON ;
        insert appSess;
    }
    //Application comment
    
    @AuraEnabled
    public static void stopApplicationSessionClock() {
        List<Contact> userContacts = [SELECT Id, Name, Related_User__c FROM Contact WHERE RecordType.Name = 'Partner Attorney' AND Related_User__c =: UserInfo.getUserId()];
        Contact userContact = userContacts[0];
        List<App_Session__c> appSessions = [SELECT Id, Name, AppSessionStartTime__c, AppSessionEndTime__c, Session_Owner__c, Session_Owner__r.Related_User__c FROM App_Session__c WHERE Session_Owner__c =: userContact.Id AND AppSessionEndTime__c = null];
        App_Session__c appSession = appSessions[0];
        appSession.AppSessionEndTime__c = dateTime.now();
        Long calcSessionTime = appSession.AppSessionEndTime__c.getTime() - appsession.AppSessionStartTime__c.getTime();
        appSession.Session_Time__c = Decimal.valueOf((calcSessionTime/1000)/60);
        update appSession;
    }
    
    
    //This method is called on click of Submit of App Settings. 
    //It pulls the User ID, and matches that PA User with open
    //Partner Attorney call types that are on Case Files where 
    //the PA_User matches current running user. It will return 
    //the LIFO Call.
    @AuraEnabled
    public static String fetchCall (String callTypesJSON) {
        system.debug('callTypesJSON='+ callTypesJSON);
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        //Grab running user's ID
        String userId = UserInfo.getUserId();
        
        //Grab current DateTime timestamp (for some reason this only works as a variable in string constructed SOQL queries).
        DateTime currentTime;
        currentTime = system.now();
        
        //Create the start of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCount query below
        String soqlStart;
        soqlStart = 'SELECT Id, Name, Attempt__c, Status__c, Earliest_Call_Time__c, Case_File__r.Partner_Attorney_User__c, Case_File__r.Primary_Contact__c, Contacted__c, Case_File__c, RecordType.Name FROM Call__c WHERE Status__c != \'Completed\' AND Case_File__r.DNC_Auto_Dial__c = false AND Case_File__r.Partner_Attorney_User__c =: userId AND Earliest_Call_Time__c <=: currentTime AND (';
        
        //Create the end of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCount query below
        String soqlEnd;
        soqlEnd = ') ORDER BY RecordType.Name DESC, Earliest_Call_Time__c DESC';  
        
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        //List<Object> callTypes = (List<Object>)JSON.deserializeUntyped(callTypesJSON);
        List<String> callTypes = (List<String>)JSON.deserialize(callTypesJSON, List<String>.class);
        system.debug('callTypes='+ callTypes);
        system.debug('callTypes Size='+ callTypes.size());
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        //Create the middle of the dynamic portion of the SOQL query.
        //TODO: Form Validation to ensure a callType is always selected
        String soqlMid;
        soqlMid = 'RecordType.Name = \''+callTypes[0]+'\' ';
        
        //For instances where more than 1 Call Type is selected loop through to append WHERE clause strings.
        if(callTypes.size() > 0){
            for (Integer i = 1; i<callTypes.size(); i++){
                soqlMid = soqlMid+' OR RecordType.Name = \''+callTypes[i]+'\' ';
            }
        }
        String soqlQuery = soqlStart+soqlMid+soqlEnd;
        system.debug('soqlQuery = '+soqlQuery);
        
        //Finally query with the constructed string <Keep this query in-sync with fetchCount query below
        List<Call__c> ccs = database.query(soqlQuery);
        
        //Next try to access the first index, if it fails return a no calls statement, otherwise return the object
        try{
            String callJSON = JSON.serialize(ccs[0]);
           }
        catch(System.ListException l)
        {
            String errMsg = l.getMessage();
            system.debug('errMsg: '+errMsg);
            if(errMsg.contains('List index out of bounds')){
                String noCallErr = 'No Calls';
                return noCallErr;
            }
        }
        
        
        //Construct JSON to store Call as object (highest ordered call)
        String callJSON = JSON.serialize(ccs[0]);

        //return Call object in JSON to JS Helper for attrubute setting
        return callJSON;
    }
    
    @AuraEnabled
    //This method is called on click of Submit of App Settings. 
    //It pulls the User ID, and matches that PA User with open
    //Partner Attorney call types that are on Case Files where 
    //the PA_User matches current running user. It will return 
    //total number of call types selected.
    //++++++++++++++++++++++++++++++
    //++++++++++++++++++++++++++++++
    public static Integer fetchCount (String callTypesJSON) {
        system.debug('callTypesJSON='+ callTypesJSON);
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        //Grab running user's ID
        String userId = UserInfo.getUserId();
        
        //Grab current DateTime timestamp (for some reason this only works as a variable in string constructed SOQL queries).
        DateTime currentTime;
        currentTime = system.now();
        
        //Create the start of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCall query above
        String soqlStart;
        soqlStart = 'SELECT Id, Name, Attempt__c, Status__c, Earliest_Call_Time__c, Case_File__r.Partner_Attorney_User__c, Case_File__r.Primary_Contact__c, Case_File__c, RecordType.Name FROM Call__c WHERE Status__c != \'Completed\' AND Contacted__c = false AND Case_File__r.Partner_Attorney_User__c =: userId AND Earliest_Call_Time__c <=: currentTime AND (';
        
        //Create the end of the non-dynamic portion of the SOQL Query <Keep this query in-sync with fetchCall query above
        String soqlEnd;
        soqlEnd = ') ORDER BY RecordType.Name DESC, Earliest_Call_Time__c DESC';  
        
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        //List<Object> callTypes = (List<Object>)JSON.deserializeUntyped(callTypesJSON);
        List<String> callTypes = (List<String>)JSON.deserialize(callTypesJSON, List<String>.class);
        system.debug('callTypes='+ callTypes);
        system.debug('callTypes Size='+ callTypes.size());
        //++++++++++++++++++++++++++++++
        //++++++++++++++++++++++++++++++
        
        //Create the middle of the dynamic portion of the SOQL query.
        String soqlMid;
        soqlMid = 'RecordType.Name = \''+callTypes[0]+'\' ';
        
        //For instances where more than 1 Call Type is selected loop through to append WHERE clause strings.
        if(callTypes.size() > 0){
            for (Integer i = 1; i<callTypes.size(); i++){
                soqlMid = soqlMid+' OR RecordType.Name = \''+callTypes[i]+'\' ';
            }
        }
        String soqlQuery = soqlStart+soqlMid+soqlEnd;
        system.debug('soqlQuery = '+soqlQuery);
        
        //Finally query with the constructed string <Keep this query in-sync with fetchCall query above
        List<Call__c> ccs =database.query(soqlQuery);
        
        //Set query size as the integer to be returned
        Integer callCount = ccs.size();
        
        //Return integer to JS Helper for assignment to cmp attribute
        return callCount;
    }
    
    @AuraEnabled
    //This method is called after submission of App Settings. 
    //It takes in an object ID and returns the SObject.
    public static Case_File__c fetchCaseFile (String caseFileId){
        Case_File__c cf = [SELECT Id, Name, PA_Friendly__c
                           FROM Case_File__c
                           WHERE Case_File__c.Id =: caseFileId
                           LIMIT 1];
        return cf;
    }
    
    @AuraEnabled
    //This method is called after submission of App Settings. 
    //It takes in an object ID and returns the SObject.
    public static Contact fetchContact (String contactId){
        Contact c =[SELECT Id, Name, Phone, MobilePhone, HomePhone, OtherPhone, DNC_Auto_Dial__c
                    FROM Contact
                    WHERE Contact.Id =: contactId
                    LIMIT 1];
        return c;
    }
    
    @AuraEnabled
    public static String GR_CallClient (String clientPhone, String attorneyPhone, String contactId){
        System.debug('****JB**** ClientPhone is: '+ clientPhone);
        System.debug('****JB**** Attorney Phone is: '+ attorneyPhone);
        System.debug('****JB**** Contact ID is: '+ contactId);
        GR_TwilioFunctionality GRTF = new GR_TwilioFunctionality();
        String callSID = GRTF.makeTwilioCall(clientPhone, attorneyPhone, contactId);
        return callSID;
    }
    
}
@IsTest
private class TwilioSoftphoneCtrl_TestClass {
    @testSetup static void setupTestData(){
    	RecordType prsRecType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType WHERE SobjectType = 'Prospect__c' AND DeveloperName = 'Bankruptcy' LIMIT 1];
    	
        Profile p = [SELECT Id FROM Profile WHERE Name='Case Manager']; 
      	// CREATING TEST USERS
        List<User> usrList = new List<User>();
        for( Integer i = 0; i < 6; i++ ){
            User u = new User(
                Alias = 'testUsr' + i, 
                Email='testUsr' + i + '@testorg.com', 
                EmailEncodingKey='UTF-8', 
                LastName='Testing' + i, 
                LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', 
                ProfileId = p.Id, 
                TimeZoneSidKey='America/Chicago',   
                UserName='testUsr' + i + '@UprightLawtestorg.com');
            usrList.add(u);
        }
        insert usrList; 
		// CREATING TEST STATES
    	State__c testState = new State__c(Name = 'Illinois', State_Entity__c = 'UpRight Law LLC', Case_Manager__c = usrList[3].Id, Chapter_13_Internal_Attorney__c = usrList[4].Id, Chapter_7_Internal_Attorney__c = usrList[5].Id ,Abbreviation__c = 'XX', Outbound_CallerID__c = '3125551212');
        insert testState;
        // CREATING TEST TIME ZONES
        Time_Zone__c tz = new Time_Zone__c(Name='America/Chicago', Standard_Offset__c = -6, DST__c = true);
	    insert tz;
        Time_Zone__c test_tz = new Time_Zone__c(Name='America/Los_Angeles', Standard_Offset__c = -8, DST__c = true);
		insert test_tz;
        // CREATING TEST ZIP CODES
        Zip_Code__c test_z1 = new Zip_Code__c(Name='60056', Time_Zone__c=tz.Id);
        insert test_z1;
        Zip_Code__c test_z2 = new Zip_Code__c(Name='90210', Time_Zone__c=test_tz.Id);
        insert test_z2;
        Call_Pattern__c testCallPattern = new Call_Pattern__c( Name = 'testCallPattern', Active__c = true);
        insert testCallPattern;
        Call_Template__c testCallTemplate = new Call_Template__c( Attempt__c = 1, Call_Pattern__c = testCallPattern.Id, Team__c = 'A');
        insert testCallTemplate;
        
        // CREATING TEST PROSPECTS
        List<Prospect__c> prospects = new List<Prospect__c>();
        for( Integer i = 0; i < 2; i++ ){
            prospects.add(new Prospect__c(
                Name = 'Test Prospect' + i,
                First_Name__c = 'Test',
                Last_Name__c = 'Prospect'+i,
                Home_Phone__c = '123-123-1234',
                Work_Phone__c = '987-987-9876',
                Mobile_SMS_Phone__c = '454-454-4545',
                Personal_Email__c = 'TestProspect' + i + '@testorg.com',
                Street__c = '1910 N Elm Street',
                City__c = 'Springfield',
                Postal_Code__c = '60056',
                State__c = 'XX', 
                RecordTypeId = prsRecType.Id));
        }
        insert prospects;
        // CREATING TEST CALLS
        List<Call__c> calls = new List<Call__c>();
        for( Prospect__c prspt : prospects ){
            for( Integer i = 0; i < 3; i++){
                calls.add(new Call__c(
                  	Status__c = 'Not Started',
                    Attempt__c  = i, 
                    Prospect__c = prspt.Id,  
                    Team__c = 'A'
                ));
            }
        }
        insert calls;
       
    }
    
    static testMethod void testTwilioAccount() {
        TwilioAccount testTA = TwilioSoftphoneCtrl.getTwilioAccount();
        //System.assertEquals( 'AC716521657c02fbe6489cf8af191ade08', testTA.getSid());
    }
    
    static testMethod void testStateOutboundCallerID() {
        String testStatePhoneNumber = TwilioSoftphoneCtrl.getStatePhoneNumber('XX');
        System.assertEquals( '3125551212', testStatePhoneNumber);
    }
    
  
    
    static testMethod void testTwilioToken() {
        String ACCOUNT_SID = 'ACe35fa95cdf61c8bb333d19b6d74c7c30';
		String AUTH_TOKEN = '30026fb2937f1a83808d0fea3f7d7cdd';
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
		TwilioCapability capability = new TwilioCapability(ACCOUNT_SID, AUTH_TOKEN);
        //capability.allowClientOutgoing();
        // String testTwilToken = capability.generateToken();
        // String testGenerateToken = TwilioSoftphoneCtrl.getTwilioToken();
    }

    
    static testMethod void testTwiMLForOutboundCall(){
        PageReference pageRef = Page.twilioOutboundCallXML;
        Test.setCurrentPage(pageRef);
        // pageRef.getParameters().put('From', '12342342345');
        // pageRef.getParameters().put('To', '12175551451');
        // pageRef.getParameters().put('Body', 'This is a test message.');
      
        ApexPages.currentPage().getParameters().put('fromNum', '+12342342345');
        ApexPages.currentPage().getParameters().put('toNum', '+12175551451');
        TwilioSoftphoneCtrl ctrl = new TwilioSoftphoneCtrl();
        String OutboundXML = ctrl.getTwimlForOutboundCall();
        
    }
    
    
    static testMethod void testTwiMLForConferenceCall(){
        PageReference pageRef = Page.twilioConferenceXML;
        Test.setCurrentPage(pageRef);
        
        TwilioSoftphoneCtrl ctrl = new TwilioSoftphoneCtrl();
        String ConferenceXML = ctrl.getTwimlForConference();
        
    }
    
    

}
public with sharing class SubmitClientController {
    @AuraEnabled
    public static void createDeal(String prospectId, String why, String hope, String debts, String notes, String callId){
        System.debug('prospectId variable: ' + prospectId);
        
        Prospect__c p = [SELECT Id, Name, Transferred__c, Lead_Campaign__c, Lead_Campaign_Sub_ID__c FROM Prospect__c WHERE Id =: prospectId LIMIT 1];
        List<RecordType> rt = [SELECT Id FROM RecordType WHERE Name = 'Bankruptcy' AND SobjectType = 'Deal__c'];
        
        System.debug('prospectId Value: '+  p.Id);
        System.debug('recordType ID: '+  rt[0].Id);
        
        Deal__c d = new Deal__c();
        d.Name = p.Name;
        d.Stage__c = 'Not Started';
        d.Prospect__c = prospectId;
        d.RecordTypeId = rt[0].Id;
        d.Hope__c = hope;
        d.Motivator__c = why;
        d.Facts_about_Debt__c = debts;
        d.First_Responder__c = UserInfo.getUserId();
        d.Call__c = callId;
        d.Lead_Campaign_Sub_ID__c = p.Lead_Campaign_Sub_ID__c;
        d.Lead_Campaign__c = p.Lead_Campaign__c;
        
        Task tsk = new task();
        tsk.Subject = 'Submitted for Consult'; 
        tsk.Status = 'Completed';
        tsk.OwnerId = UserInfo.getUserId();
        tsk.WhatId = prospectId;
        tsk.Call__c = callId;
        tsk.ActivityDate = date.today(); 
        tsk.Description = notes;
        
        insert tsk;
        insert d;
        
        //Update Calls and Consults stats for Call_Pattern__c
        Call__c call = [SELECT Call_Pattern__c, Status__c, Transferred__c FROM Call__c WHERE Id =: callId];
        
                
        //Update status of submitted consult's call to Completed.
        call.Status__c = 'Completed';
        call.Completed_By__c = UserInfo.getUserId();
        call.Completed__c = System.now();
        call.Transferred__c = System.now();
        update call;
        
        p.Transferred__c = System.now();
        update p;
        
        Call_Pattern__c pattern = [SELECT Id, Calls__c, Consults__c FROM Call_Pattern__c WHERE Id =: call.Call_Pattern__c];
        
        if (pattern.Calls__c == null){
            pattern.Calls__c = 1;
        }
        else{
        	pattern.Calls__c++;    
        }
       
        if (pattern.Consults__c == null){
            pattern.Consults__c = 1;
        }
        else{
            pattern.Consults__c++;    
        }        
        
        update pattern;
        
    }
    @AuraEnabled 
    public static Prospect__c grabInfo(String prospectId){
        System.debug('prospectId variable in grabInfo method: ' + prospectId);
        Prospect__c pr = [SELECT Id, First_Name__c, Last_Name__c, Home_Phone__c, Work_Phone__c, Mobile_SMS_Phone__c, Personal_Email__c, State__c, Postal_Code__c FROM Prospect__c WHERE Id =: prospectId LIMIT 1];
        
        return pr;
    }
    @AuraEnabled 
    public static void postReqToDPP(String url){
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody('url='+EncodingUtil.urlEncode(url, 'UTF-8'));
        req.setCompressed(true);
        
        try {
            res = http.send(req);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
        
    }
}